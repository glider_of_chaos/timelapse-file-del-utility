import os
import shutil




# here we'll get list of files in 'path' directory.
# Note: subdirectories are not icluded
def build_file_list(path, flist = None):
	print("Geting list of files from ")
	print(path)
	if flist == None:													# is '==' ok?
		flist=[]
	for dname in os.listdir(path):
		file_path1=os.path.join(path, dname)
		if os.path.isfile(file_path1):
			flist.append(file_path1)
# result sorted by modification date:
#	return sorted(flist, key=os.path.getmtime)

# result sorted by modification path:
	return sorted(flist)
	
def filter_list_by_ending(f1, le1):
	print("Removing files ending with ")
	print(le1)
	f2=[]
	for s1 in f1:
		f2.append(s1)
		for s2 in le1:
			if s2 == s1[-len(s2):]:
				f2.remove(s1)
				break
	return f2

def get_number_to_skip():
	while True:
		try:
			n=int(input('Please enter N (every N-th file will be copied: '))
			break
		except:
			pass
	return n


#list of 'end of name' strings to block from copying
lblockedendstrings = ['.py', '.tmp','db']

#lets get our script dir
script_path = os.getcwd()
print(script_path)
selection_path = str(script_path) + '/selection/'

flist = build_file_list(script_path)
flist = filter_list_by_ending(flist, lblockedendstrings)
n = get_number_to_skip()

if not os.path.exists(selection_path):
	print("creating directory ")
	print(selection_path)
	os.makedirs(selection_path)
else:
	print("====directory exists====")

print('copying files')
i = 0
while i < len(flist):
	print('.', end="")
	shutil.copy(flist[i],selection_path)
	i=i+n
input("\nDone. Press Enter to continue...")